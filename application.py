from flask import Flask

application = Flask(__name__)


@application.route('/')
def hello_world():
    return 'Hello, World!'


@application.route('/<name>')
def hello_you(name):
    return f"Hello, {name.capitalize()}!"


if __name__ == '__main__':
    application.run(debug=True, port=8080)
