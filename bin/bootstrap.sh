#!/bin/bash
# Fail on errors
set -e

# Install AWS and AWS Elastic Beanstalk CLI tools if needed
command -v aws >/dev/null || pip install awscli
command -v eb >/dev/null || pip install awsebcli

# Use the admin profile by default
export AWS_PROFILE=admin

# Create group with permissions to Elastic Beanstalk and Elastic Load Balancing
aws iam create-group --group-name flask-starter-group
aws iam attach-group-policy --group-name flask-starter-group --policy-arn arn:aws:iam::aws:policy/AWSElasticBeanstalkFullAccess
aws iam attach-group-policy --group-name flask-starter-group --policy-arn arn:aws:iam::aws:policy/ElasticLoadBalancingFullAccess

# Create a user and attach it to the above group
aws iam create-user --user-name flask-starter-user
aws iam add-user-to-group --user-name flask-starter-user --group-name flask-starter-group

# Create the Elastic Beanstalk app and environment
eb init flask-starter --platform python-3.6 --region us-east-1
eb create flask-starter-env
eb deploy
eb open
